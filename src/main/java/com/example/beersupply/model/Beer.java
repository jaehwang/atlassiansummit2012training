package com.example.beersupply.model;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
public class Beer
{
    private String issueKey;
    private String name;
    private String style;
    private String description;
    private String labelUrl;
    private Double averageRating;
    private Double userRating;

    public Beer(String issueKey, String name, String style, String description, String labelUrl, Double averageRating, Double userRating)
    {
        this.issueKey = issueKey;
        this.name = name;
        this.style = style;
        this.description = description;
        this.labelUrl = labelUrl;
        this.averageRating = averageRating;
        this.userRating = userRating;
    }

    public String getIssueKey()
    {
        return issueKey;
    }

    public String getName()
    {
        return name;
    }

    public String getStyle()
    {
        return style;
    }

    public String getDescription()
    {
        return description;
    }

    public String getLabelUrl()
    {
        return labelUrl;
    }

    public Double getAverageRating()
    {
        return averageRating;
    }

    public Double getUserRating()
    {
        return userRating;
    }
}
