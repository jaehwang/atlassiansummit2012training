package com.example.beersupply.brewerydb.representations;

/**
 * @since version
 */
public class Style
{
    private String name;
    private String description;

    public String getName()
    {
        return (null != name) ? name : "";
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return (null != description) ? description : "";
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
