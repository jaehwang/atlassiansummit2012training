package com.example.beersupply.brewerydb.representations;

import java.util.List;

/**
 * @since version
 */
public class StylesCollection
{
    private List<Style> data;

    public List<Style> getData()
    {
        return data;
    }
}
