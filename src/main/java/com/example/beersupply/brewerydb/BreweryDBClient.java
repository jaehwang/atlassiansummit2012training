package com.example.beersupply.brewerydb;

import java.util.List;

import com.example.beersupply.brewerydb.representations.BeerDescriptor;
import com.example.beersupply.brewerydb.representations.Style;

/**
 * @since version
 */
public interface BreweryDBClient
{
    public static final String BASE_URL = "http://api.brewerydb.com/v2";
    public static final String BEER_STYLES_URL = BASE_URL + "/styles";
    public static final String BEER_SEARCH_URL = BASE_URL + "/search";
    

    List<Style> getAllBeerStyles(String apiKey);
    List<BeerDescriptor> search(String searchTerm, String apiKey);
}
