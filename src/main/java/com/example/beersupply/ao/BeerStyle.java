package com.example.beersupply.ao;


import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Unique;

/**
 * This is an ActiveObjects Entity representing a beer style.
 * It will be used to populate the beer style custom field options.
 */
@Preload //tell AO to preload our value since we always want the name
public interface BeerStyle extends Entity
{
    @NotNull //must have a value
    @Indexed //index the names
    @Unique //must be unique
    @StringLength(StringLength.MAX_LENGTH) //max length is 767
    public String getStyleName();
    public void setStyleName(String name);
}
