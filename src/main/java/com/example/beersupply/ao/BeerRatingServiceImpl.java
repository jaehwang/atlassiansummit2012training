package com.example.beersupply.ao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.activeobjects.external.ActiveObjects;

import com.google.common.collect.ImmutableList;

import com.google.common.collect.ImmutableSet;
import net.java.ao.DBParam;
import net.java.ao.EntityStreamCallback;
import net.java.ao.Query;

/**
 * @since version
 */
public class BeerRatingServiceImpl implements BeerRatingService
{
    private final ActiveObjects ao;

    public BeerRatingServiceImpl(ActiveObjects ao)
    {
        this.ao = ao;
    }

    @Override
    public BeerRating add(String issueKey, String userName, Double rating)
    {
        return null;
        // Summit 2012 training: Implement me!
    }

    @Override
    public void delete(String issueKey, String username)
    {
        // Summit 2012 training: Implement me!
    }

    @Override
    public Iterable<String> getVotingUsers() {
        return null;
        // Summit 2012 Training: Implement me!
    }

    @Override
    public List<BeerRating> getRatingsForIssue(String issueKey)
    {
        final List<BeerRating> ratings = new ArrayList<BeerRating>();

        ao.stream(BeerRating.class, Query.select("*").where("issue_key = ?", issueKey),new EntityStreamCallback<BeerRating, Integer>() {
            @Override
            public void onRowRead(BeerRating rating)
            {
                ratings.add(rating);
            }
        });

        return ImmutableList.copyOf(ratings);
    }

    @Override
    public BeerRating getRatingForIssueByUser(String issueKey, String username)
    {
        BeerRating[] ratings = ao.find(BeerRating.class, Query.select().where("issue_key = ? AND username = ?",issueKey,username).limit(1));

        if(null != ratings && ratings.length > 0)
        {
            return ratings[0];
        }

        return null;
    }

    @Override
    public BeerRating getById(Integer id)
    {
        return ao.get(BeerRating.class,id);
    }

    @Override
    public int getCount()
    {
        return ao.count(BeerRating.class);
    }

    @Override
    public int getCountByIssue(String issueKey)
    {
        return ao.count(BeerRating.class,Query.select().where("issue_key = ?", issueKey));
    }

    @Override
    public Double getSumByIssue(String issueKey)
    {
        List<BeerRating> ratings = getRatingsForIssue(issueKey);
        Double sum = 0.0;

        for(BeerRating rating : ratings)
        {
            sum += rating.getRating();
        }

        return sum;
    }
}
