package com.example.beersupply.ao;

import java.util.List;

import com.atlassian.activeobjects.tx.Transactional;

/**
 * This is a service that wraps ActiveObjects to make it easy to retrieve and store {@link BeerStyle} entities
 */
@Transactional //run everything in a transaction
public interface BeerStyleService
{
    /**
     * The database parameter name for the BeerStyle styleName.
     * This is essentially the name of the field, all uppercase with _ separating camel-casing
     */
    public static final String DBPARAM_STYLE_NAME = "STYLE_NAME";

    /**
     * Adds a new BeerStyle to the database
     * @param styleName
     * @return the newly created BeerStyle
     */
    BeerStyle add(String styleName);

    /**
     * Gets all of the styles in the database
     * @return a List of the styles
     */
    List<BeerStyle> getAll();

    /**
     * Gets a BeerStyle by it's name
     * @param name
     * @return a BeerStyle
     */
    BeerStyle getByName(String name);

    /**
     * Gets a BeerStyle by it's database id
     * @param id
     * @return A BeerStyle
     */
    BeerStyle getById(Integer id);

    int getCount();
}
