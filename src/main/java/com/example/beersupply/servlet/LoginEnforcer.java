package com.example.beersupply.servlet;

import java.io.IOException;
import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A helper class for {@link ServletHandler}s that need to enforce admin rights
 */
public class LoginEnforcer
{
    /**
     * the url param JIRA uses to redirect to after a successful login
     */
    static final String JIRA_SERAPH_SECURITY_ORIGINAL_URL = "os_security_originalurl";

    /**
     * Provides the URI for the login screen in JIRA.
     * injected via a component-import in our plugin.xml
     */
    private final LoginUriProvider loginUriProvider;

    /**
     * Allows us to enable web-sudo support in our plugin
     * injected via a component-import in our plugin.xml
     */
    private final WebSudoManager webSudoManager;

    /**
     * We use this to check that the current user is an admin
     * injected via a component-import in our plugin.xml
     */
    private final UserManager userManager;

    public LoginEnforcer(LoginUriProvider loginUriProvider, WebSudoManager webSudoManager, UserManager userManager)
    {
        this.loginUriProvider = checkNotNull(loginUriProvider);
        this.webSudoManager = checkNotNull(webSudoManager);
        this.userManager = checkNotNull(userManager);
    }

    /**
     * Subclasses can call this to easily ensure an admin is logged in.
     *
     * @param request
     * @param response
     * @throws IOException
     */
    public boolean requiresLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        boolean needsLogin = false;
        try
        {
            webSudoManager.willExecuteWebSudoRequest(request);
            if(!isAdmin(request))
            {
                needsLogin = true;
                redirectToLogin(request,response);
            }
        }
        catch (WebSudoSessionException wse)
        {
            webSudoManager.enforceWebSudoProtection(request, response);
        }

        return needsLogin;
    }

    /**
     * A simple check to see if the currently logged in user is an admin
     *
     * @return
     */
    public boolean isAdmin(HttpServletRequest request)
    {
        String username = userManager.getRemoteUsername(request);
        if (username == null)
        {
            return false;
        }
        return userManager.isSystemAdmin(username) || userManager.isAdmin(username);
    }

    /**
     * Adds our url to the session and redirects to the login page
     *
     * @param request
     * @param response
     * @throws IOException
     */
    public void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        //get our url and add it to the session so JIRA will send us back here after the user logs in.
        final URI uri = getUri(request);
        request.getSession().setAttribute(JIRA_SERAPH_SECURITY_ORIGINAL_URL, uri.toASCIIString());

        response.sendRedirect(loginUriProvider.getLoginUri(uri).toASCIIString());
    }

    public URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

}
