package com.example.beersupply.servlet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

/**
 * Enum used by {@link ServletHandler}s to map request paths to result templates
 */
public enum ServletMapping
{
    ADMIN_SCREEN("beersupplyadmin","beer-supply-admin.vm")
    ,ADMIN_RUN_SETUP("runsetup","beer-supply-admin.vm")
    ,ADMIN_SET_APIKEY("setapikey","beer-supply-admin.vm")
    ,VIEW_BEERS("beerview","beer-supply-view.vm")
    ,ERROR("","error.vm")
    ,NOT_MAPPED("","");

    public static final String TEMPLATE_PREFIX = "/templates/servlets/";

    private String path;
    private String resultTemplate;

    private ServletMapping(String path, String template)
    {
        this.path = path;
        this.resultTemplate = TEMPLATE_PREFIX + template;
    }

    public String getPath()
    {
        return path;
    }

    public String getResultTemplate()
    {
        return resultTemplate;
    }

    public static ServletMapping fromPath(String path)
    {
        for(ServletMapping mapping : ServletMapping.values())
        {
            if(mapping.getPath().equals(path))
            {
                return mapping;
            }
        }

        return NOT_MAPPED;
    }

    public static ServletMapping fromPath(HttpServletRequest request)
    {
        return fromPath(getMappingPath(request));
    }

    private static String getMappingPath(HttpServletRequest request)
    {
        String lastPath = StringUtils.substringAfterLast(request.getRequestURI(), "/");
        if (StringUtils.isBlank(lastPath))
        {
            lastPath = StringUtils.substringAfterLast(StringUtils.substringBeforeLast(request.getRequestURI(), "/"),"/");
        }

        return lastPath;
    }
}
