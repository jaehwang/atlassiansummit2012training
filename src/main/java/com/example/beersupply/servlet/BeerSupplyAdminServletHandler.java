package com.example.beersupply.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.example.beersupply.components.BeerSupplyConfigurationManager;
import com.example.beersupply.exception.ExceptionWithErrorCollection;

import org.apache.commons.lang.StringUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since version
 */
public class BeerSupplyAdminServletHandler implements ServletHandler
{
    public static final String PARAM_API_KEY = "apiKey";
    
    private final LoginEnforcer loginEnforcer;

    private final TemplateRenderer renderer;

    private final BeerSupplyConfigurationManager configurationManager;

    private final JiraAuthenticationContext authContext;

    private final I18nResolver i18n;

    private final VelocityRequestContextFactory requestContextFactory;

    public BeerSupplyAdminServletHandler(LoginEnforcer loginEnforcer, TemplateRenderer renderer, BeerSupplyConfigurationManager configurationManager, JiraAuthenticationContext authContext, I18nResolver i18n, VelocityRequestContextFactory requestContextFactory)
    {
        this.loginEnforcer = checkNotNull(loginEnforcer);
        this.renderer = checkNotNull(renderer);
        this.configurationManager = configurationManager;
        this.authContext = authContext;
        this.i18n = i18n;
        this.requestContextFactory = requestContextFactory;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        //make sure the user is an admin, if not just return so the redirect will be processed.
        if(loginEnforcer.requiresLogin(request, response))
        {
            return;
        }

        //set our content type
        response.setContentType("text/html;charset=utf-8");

        //create our velocity context
        final Map<String, Object> context = new HashMap<String, Object>();

        context.put("requestContext", requestContextFactory.getJiraVelocityRequestContext());

        //get our path->result template mapping
        ServletMapping mapping = ServletMapping.fromPath(request);

        try
        {
            switch (mapping)
            {
                case ADMIN_SCREEN:
                    handleAdminScreen(request, response, context, mapping.getResultTemplate());
                    break;
                case ADMIN_RUN_SETUP:
                    handleRunSetup(request, response, context, mapping.getResultTemplate());
                    break;
                case ADMIN_SET_APIKEY:
                    handleSetApiKey(request, response, context, mapping.getResultTemplate());
                    break;
                case NOT_MAPPED:
                    handleAdminScreen(request, response, context, mapping.getResultTemplate());
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            ErrorCollection errorCollection;
            if(e instanceof ExceptionWithErrorCollection)
            {
                errorCollection = ((ExceptionWithErrorCollection)e).getErrorCollection();
            }
            else
            {
                String error = (e.getMessage() != null)? e.getMessage() : e.getClass().getName();
                String message = "Error: " + error;
                errorCollection = new SimpleErrorCollection();
                errorCollection.addErrorMessage(message);
            }

            String errorTemplate = ServletMapping.ERROR.getResultTemplate();
            context.put("errorCollection", errorCollection);

            renderer.render(errorTemplate, context, response.getWriter());
        }
    }


    private void handleAdminScreen(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, String resultTemplate) throws Exception
    {
        String apiKey = (configurationManager.getBreweryDBApiKey() != null) ? configurationManager.getBreweryDBApiKey() : "";
        context.put(PARAM_API_KEY,apiKey);
        
        context.put("needsConfiguration",!configurationManager.isProjectConfigured());

        renderer.render(resultTemplate, context, response.getWriter());
    }

    private void handleRunSetup(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, String resultTemplate) throws Exception
    {
        if(!configurationManager.isProjectConfigured())
        {
            configurationManager.runSetup(authContext.getLoggedInUser());
            context.put("successMessage",i18n.getText("beer-supply.setup.success"));
        }

        String apiKey = (String)request.getParameter(PARAM_API_KEY);
        if(StringUtils.isNotBlank(apiKey))
        {
            configurationManager.setBreweryDBApiKey(apiKey);
            context.put(PARAM_API_KEY,apiKey);
        }

        renderer.render(resultTemplate, context, response.getWriter());
    }

    private void handleSetApiKey(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, String resultTemplate) throws Exception
    {
        String apiKey = (String)request.getParameter(PARAM_API_KEY);
        if(StringUtils.isNotBlank(apiKey))
        {
            configurationManager.setBreweryDBApiKey(apiKey);
            context.put(PARAM_API_KEY,apiKey);
            context.put("successMessage",i18n.getText("beer-supply.api.key.saved"));
        }
        else
        {
            context.put(PARAM_API_KEY,"");
        }

        renderer.render(resultTemplate,context,response.getWriter());
    }
}
