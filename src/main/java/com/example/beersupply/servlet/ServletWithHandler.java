package com.example.beersupply.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This is the actual servlet implementation for servlets with {@link ServletHandler}s injected
 * Subclasses should use the {@link org.springframework.beans.factory.annotation.Qualifier} annotation to specify which handler they want injected
 */
public class ServletWithHandler extends HttpServlet
{
    private final ServletHandler handler;

    public ServletWithHandler(ServletHandler handler)
    {
        this.handler = checkNotNull(handler,"handler");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handler.handle(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        handler.handle(request,response);
    }
}
