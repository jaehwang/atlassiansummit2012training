package com.example.beersupply.jira.customfields;

import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.impl.ReadOnlyCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;

/**
 * @since version
 */
public class BreweryDBSearchCFType extends GenericTextCFType
{
    public static final String CF_KEY = "com.example.beersupply.atlassian-beer-supply:beer-supply-brewerydb-search-cf";
    protected BreweryDBSearchCFType(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager)
    {
        super(customFieldValuePersister, genericConfigManager);
    }
}
