package com.example.beersupply.jira.customfields;

import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.AbstractSingleFieldType;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.impl.TextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;

import com.example.beersupply.ao.BeerStyle;
import com.example.beersupply.ao.BeerStyleService;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

public class BeerStyleCFType extends GenericTextCFType
{
    public static final String CF_KEY = "com.example.beersupply.atlassian-beer-supply:beer-supply-beer-style-cf";
    public static final String SEARCHER_KEY = "com.example.beersupply.atlassian-beer-supply:beer-style-searcher";

    /**
     * Gets injected
     */
    private final BeerStyleService beerStyleService;

    public BeerStyleCFType(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager, BeerStyleService beerStyleService)
    {
        super(customFieldValuePersister, genericConfigManager);
        this.beerStyleService = checkNotNull(beerStyleService);
    }

    @Override
    public Map<String, Object> getVelocityParameters(final Issue issue, final CustomField field, final FieldLayoutItem fieldLayoutItem)
    {
        final Map<String, Object> map = super.getVelocityParameters(issue, field, fieldLayoutItem);

        //add our beer styles to the context
        List<String> styleNames = Lists.transform(beerStyleService.getAll(),new Function<BeerStyle, String>() {
            @Override
            public String apply(@Nullable BeerStyle input)
            {
                if(null != input)
                {
                    return input.getStyleName();
                }
                else
                {
                    return "";
                }
            }
        });

        map.put("beerStyles", Joiner.on(",").join(styleNames));

        return map;
    }

    /**
     * Overriding this method so we can check if a new style was entered and add it to the db if needed
     */
    @Override
    public void createValue(CustomField field, Issue issue, String value)
    {
        super.createValue(field, issue, value);

        createStyleIfNeeded(value);
    }

    /**
     * Overriding this method so we can check if a new style was entered and add it to the db if needed
     */
    @Override
    public void updateValue(CustomField customField, Issue issue, String value)
    {
        super.updateValue(customField, issue, value);

        createStyleIfNeeded(value);
    }

    /**
     * Creates the style in the database if it's not already there
     * @param styleName
     */
    private void createStyleIfNeeded(String styleName)
    {
        if(null == beerStyleService.getByName(styleName))
        {
            beerStyleService.add(styleName);
        }
    }
}