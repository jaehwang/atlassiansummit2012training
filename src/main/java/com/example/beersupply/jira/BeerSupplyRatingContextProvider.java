package com.example.beersupply.jira;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.message.I18nResolver;

import com.example.beersupply.components.BeerRatingManager;
import com.example.beersupply.jira.customfields.BeerAverageRatingCFType;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

/**
 * @since version
 */
public class BeerSupplyRatingContextProvider implements ContextProvider
{
    private final JiraAuthenticationContext authContext;
    private final CustomFieldManager customFieldManager;
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final VelocityRequestContextFactory requestContextFactory;
    private final BeerRatingManager ratingManager;

    public BeerSupplyRatingContextProvider(JiraAuthenticationContext authContext, CustomFieldManager customFieldManager, WebResourceUrlProvider webResourceUrlProvider, VelocityRequestContextFactory requestContextFactory, BeerRatingManager ratingManager)
    {
        this.authContext = authContext;
        this.customFieldManager = customFieldManager;
        this.webResourceUrlProvider = webResourceUrlProvider;
        this.requestContextFactory = requestContextFactory;
        this.ratingManager = ratingManager;
    }


    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context)
    {
        final Issue issue = (Issue) context.get("issue");
        final User user = authContext.getLoggedInUser();

        Double userRating = ratingManager.getUserRating(issue.getKey(),user);

        CustomField averageRatingField = Iterables.find(customFieldManager.getCustomFieldObjects(issue), new Predicate<CustomField>() {
            @Override
            public boolean apply(@Nullable CustomField input)
            {
                if(null != input && input.getCustomFieldType() instanceof BeerAverageRatingCFType)
                {
                    return true;
                }
                return false;
            }
        });

        Double average = (Double)issue.getCustomFieldValue(averageRatingField);

        if(null == average)
        {
            average = new Double(0);
        }

        String beerIconPath=webResourceUrlProvider.getStaticPluginResourceUrl("com.example.beersupply.atlassian-beer-supply:beer-supply-resources", "", UrlMode.ABSOLUTE);

        Map<String,Object> newContext = new HashMap<String, Object>(context);

        newContext.put("requestContext", requestContextFactory.getJiraVelocityRequestContext());
        newContext.put("issueKey",issue.getKey());
        newContext.put("averageRating",average);
        newContext.put("userRating",userRating);
        newContext.put("beer-icon-path",beerIconPath);



        return newContext;
    }
}
