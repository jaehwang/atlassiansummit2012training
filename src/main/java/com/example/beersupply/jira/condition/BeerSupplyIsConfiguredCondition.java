package com.example.beersupply.jira.condition;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import com.example.beersupply.components.BeerSupplyConfigurationManager;

/**
 * @since version
 */
public class BeerSupplyIsConfiguredCondition implements Condition
{
    private final BeerSupplyConfigurationManager configurationManager;

    public BeerSupplyIsConfiguredCondition(BeerSupplyConfigurationManager configurationManager)
    {
        this.configurationManager = configurationManager;
    }

    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException
    {
        //we don't have any params
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> stringObjectMap)
    {
        return configurationManager.isProjectConfigured();
    }
}
