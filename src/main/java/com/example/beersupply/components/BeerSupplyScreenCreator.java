package com.example.beersupply.components;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.*;
import com.atlassian.jira.issue.fields.screen.*;
import com.atlassian.jira.issue.fields.screen.issuetype.*;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.operation.IssueOperations;
import com.atlassian.jira.issue.operation.ScreenableIssueOperation;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.sal.api.message.I18nResolver;

import com.example.beersupply.exception.BeerSupplySetupException;
import com.example.beersupply.jira.customfields.BeerAverageRatingCFType;
import com.example.beersupply.jira.customfields.BeerLabelCFType;
import com.example.beersupply.jira.customfields.BeerStyleCFType;
import com.example.beersupply.jira.customfields.BreweryDBSearchCFType;

import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericValue;

/**
 * @since version
 */
public class BeerSupplyScreenCreator
{
    private final I18nResolver i18n;
    private final CustomFieldManager customFieldManager;
    private final FieldLayoutManager fieldLayoutManager;
    private final ConstantsManager constantsManager;
    private final FieldScreenManager fieldScreenManager;
    private final FieldScreenSchemeManager fieldScreenSchemeManager;
    private final IssueTypeScreenSchemeManager issueTypeScreenSchemeManager;

    public BeerSupplyScreenCreator(I18nResolver i18n, CustomFieldManager customFieldManager, FieldLayoutManager fieldLayoutManager, ConstantsManager constantsManager, FieldScreenManager fieldScreenManager, FieldScreenSchemeManager fieldScreenSchemeManager, IssueTypeScreenSchemeManager issueTypeScreenSchemeManager)
    {
        this.i18n = i18n;
        this.customFieldManager = customFieldManager;
        this.fieldLayoutManager = fieldLayoutManager;
        this.constantsManager = constantsManager;
        this.fieldScreenManager = fieldScreenManager;
        this.fieldScreenSchemeManager = fieldScreenSchemeManager;
        this.issueTypeScreenSchemeManager = issueTypeScreenSchemeManager;
    }

    public void createScreenAndSchemes(Project project) throws BeerSupplySetupException
    {
        try
        {
            IssueType issueType = project.getIssueTypes().iterator().next();

            //create custom fields
            String beerStyleCFName = i18n.getText("beer-supply.beer-style.field.name");
            String beerStyleCFDesc = i18n.getText("beer-supply.beer-style.field.desc");
            String beerLabelCFName = i18n.getText("beer-supply.beer-label.field.name");
            String beerLabelCFDesc = i18n.getText("beer-supply.beer-label.field.desc");
            String averageRatingCFName = i18n.getText("beer-supply.average-rating.field.name");
            String averageRatingCFDesc = i18n.getText("beer-supply.average-rating.field.desc");
            String breweryDBSearchCFName = i18n.getText("beer-supply.brewerydb-search-field.name");
            String breweryDBSearchCFDesc = i18n.getText("beer-supply.brewerydb-search-field.desc");

            CustomField beerStyleField = createCustomField(BeerStyleCFType.CF_KEY, BeerStyleCFType.SEARCHER_KEY, beerStyleCFName, beerStyleCFDesc, issueType);
            CustomField beerLabelField = createCustomField(BeerLabelCFType.CF_KEY, null, beerLabelCFName, beerLabelCFDesc, issueType);
            CustomField averageRatingField = createCustomField(BeerAverageRatingCFType.CF_KEY, BeerAverageRatingCFType.SEARCHER_KEY, averageRatingCFName, averageRatingCFDesc, issueType);
            CustomField breweryDBSearchField = createCustomField(BreweryDBSearchCFType.CF_KEY, null, breweryDBSearchCFName, breweryDBSearchCFDesc, issueType);

            //create a default field layout. this will determine what fields are required and/or visible on which screens
            String filedLayoutName = i18n.getText("beer-supply.field.layout.name");
            String filedLayoutDesc = i18n.getText("beer-supply.field.layout.desc");

            FieldLayout fieldLayout = createFieldLayout(filedLayoutName,filedLayoutDesc,beerStyleField.getId(),beerLabelField.getId(),breweryDBSearchField.getId());

            //create the field configuration scheme
            //this  associates our field configurations to issue types
            String filedLayoutSchemeName = i18n.getText("beer-supply.field.layout.scheme.name");
            String filedLayoutSchemeDesc = i18n.getText("beer-supply.field.layout.scheme.desc");

            FieldLayoutScheme fieldLayoutScheme = createFieldLayoutScheme(filedLayoutSchemeName, filedLayoutSchemeDesc, fieldLayout.getId());

            //create our screen
            String createScreenName = i18n.getText("beer-supply.create.screen.name");
            String createScreenDesc = i18n.getText("beer-supply.create.screen.desc");
            String viewEditScreenName = i18n.getText("beer-supply.view.screen.name");
            String viewEditScreenDesc = i18n.getText("beer-supply.view.screen.desc");

            FieldScreen createIssueScreen = createScreen(createScreenName,createScreenDesc,breweryDBSearchField.getId(),IssueFieldConstants.SUMMARY,beerStyleField.getId(),IssueFieldConstants.DESCRIPTION,beerLabelField.getId(),averageRatingField.getId());
            FieldScreen viewEditIssueScreen = createScreen(viewEditScreenName,viewEditScreenDesc,IssueFieldConstants.SUMMARY,beerStyleField.getId(),IssueFieldConstants.DESCRIPTION,beerLabelField.getId(),averageRatingField.getId());
            
            //create a screen scheme to map the view/edit screen as the default
            String screenSchemeName = i18n.getText("beer-supply.screen.scheme.name");
            String screenSchemeDesc = i18n.getText("beer-supply.screen.scheme.desc");
            FieldScreenScheme screenScheme = createScreenScheme(screenSchemeName, screenSchemeDesc, viewEditIssueScreen.getId());
            
            //map the create issue screen
            addScreenToScheme(screenScheme, IssueOperations.CREATE_ISSUE_OPERATION,createIssueScreen.getId());

            //create the issue type screen scheme to choose what Screen Scheme is used for each issue type.
            String issueTypeScreenSchemeName = i18n.getText("beer-supply.issue-type.screen-scheme.name");
            String issueTypeScreenSchemeDesc = i18n.getText("beer-supply.issue-type.screen-scheme.desc");
            IssueTypeScreenScheme issueTypeScreenScheme = createIssueTypeScreenScheme(issueTypeScreenSchemeName,issueTypeScreenSchemeDesc,screenScheme.getId());

            //associate these with our project
            fieldLayoutManager.addSchemeAssociation(project.getGenericValue(),fieldLayoutScheme.getId());
            issueTypeScreenSchemeManager.addSchemeAssociation(project,issueTypeScreenScheme);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            SimpleErrorCollection errorCollection = new SimpleErrorCollection();
            errorCollection.addErrorMessage(e.getMessage());
            throw new BeerSupplySetupException(i18n.getText("beer-supply.project.creation.error"),errorCollection);
        }

    }

    private CustomField createCustomField(String cfKey, String searcherKey, String name, String desc, IssueType issueType) throws Exception
    {
        CustomFieldType cfType = customFieldManager.getCustomFieldType(cfKey);
        CustomFieldSearcher searcher = null;

        if(StringUtils.isNotBlank(searcherKey))
        {
            searcher = customFieldManager.getCustomFieldSearcher(searcherKey);
        }

        CustomField field = customFieldManager.createCustomField(name, desc, cfType, searcher, Arrays.asList(GlobalIssueContext.getInstance()), Arrays.asList(issueType.getGenericValue()));

        return field;
    }

    private FieldLayout createFieldLayout(String name, String desc, String... customFieldIds)
    {
        List<String> visibleFieldIds = new ArrayList<String>(Arrays.asList(customFieldIds));
        visibleFieldIds.addAll(Arrays.asList(IssueFieldConstants.SUMMARY, IssueFieldConstants.ISSUE_TYPE, IssueFieldConstants.REPORTER, IssueFieldConstants.COMMENT, IssueFieldConstants.DESCRIPTION));

        List<FieldLayoutItem> defaultItems = fieldLayoutManager.getEditableDefaultFieldLayout().getFieldLayoutItems();

        List<FieldLayoutItem> ourItems = new ArrayList<FieldLayoutItem>(defaultItems.size());

        for(FieldLayoutItem item : defaultItems)
        {
            if(!visibleFieldIds.contains(item.getOrderableField().getId()))
            {
                ourItems.add(new FieldLayoutItemImpl.Builder(item).setHidden(true).build());
            }
            else
            {
                ourItems.add(item);
            }
        }

        final EditableFieldLayout editableFieldLayout = new EditableFieldLayoutImpl(null, ourItems);
        editableFieldLayout.setName(name);
        editableFieldLayout.setDescription(desc);

        return fieldLayoutManager.storeAndReturnEditableFieldLayout(editableFieldLayout);
    }

    private FieldLayoutScheme createFieldLayoutScheme(String layoutSchemeName, String layoutSchemeDesc, Long fieldLayoutId)
    {
        final FieldLayoutScheme fieldLayoutScheme = new FieldLayoutSchemeImpl(fieldLayoutManager, null);
        fieldLayoutScheme.setName(layoutSchemeName);
        fieldLayoutScheme.setDescription(layoutSchemeDesc);
        fieldLayoutScheme.store();

        // Make a default entry for the scheme
        final FieldLayoutSchemeEntity fieldLayoutSchemeEntity = new FieldLayoutSchemeEntityImpl(fieldLayoutManager, null, constantsManager);
        fieldLayoutSchemeEntity.setIssueTypeId(null);
        fieldLayoutSchemeEntity.setFieldLayoutId(fieldLayoutId);
        fieldLayoutScheme.addEntity(fieldLayoutSchemeEntity);
        fieldLayoutScheme.store();

        return  fieldLayoutScheme;
    }

    private FieldScreen createScreen(String name, String desc, String... fieldIds)
    {
        FieldScreen fieldScreen =  new FieldScreenImpl(fieldScreenManager);
        fieldScreen.setName(name);
        fieldScreen.setDescription(desc);
        fieldScreen.store();

        FieldScreenTab defaultTab = fieldScreen.addTab(i18n.getText("admin.field.screen.default"));

        for(String fieldId : fieldIds)
        {
            defaultTab.addFieldScreenLayoutItem(fieldId);
        }

        return fieldScreen;
    }

    private FieldScreenScheme createScreenScheme(String name, String desc, Long screenId)
    {
        FieldScreenSchemeImpl fieldScreenScheme = new FieldScreenSchemeImpl(fieldScreenSchemeManager, null);
        fieldScreenScheme.setName(name);
        fieldScreenScheme.setDescription(desc);
        fieldScreenScheme.store();

        // Create a default scheme operation mapping
        FieldScreenSchemeItemImpl fieldScreenSchemeItem = new FieldScreenSchemeItemImpl(fieldScreenSchemeManager, (GenericValue) null, fieldScreenManager);
        
        addScreenToScheme(fieldScreenScheme,null,screenId);

        return fieldScreenScheme;
    }
    
    private void addScreenToScheme(FieldScreenScheme scheme, ScreenableIssueOperation operation, Long screenId)
    {
        FieldScreenSchemeItemImpl fieldScreenSchemeItem = new FieldScreenSchemeItemImpl(fieldScreenSchemeManager, (GenericValue) null, fieldScreenManager);
        fieldScreenSchemeItem.setIssueOperation(operation);
        fieldScreenSchemeItem.setFieldScreen(fieldScreenManager.getFieldScreen(screenId));
        
        scheme.addFieldScreenSchemeItem(fieldScreenSchemeItem);
    }

    private IssueTypeScreenScheme createIssueTypeScreenScheme(String name, String desc, Long screenSchemeId)
    {
        final IssueTypeScreenScheme issueTypeScreenScheme = new IssueTypeScreenSchemeImpl(issueTypeScreenSchemeManager, null);
        issueTypeScreenScheme.setName(name);
        issueTypeScreenScheme.setDescription(desc);
        issueTypeScreenScheme.store();

        IssueTypeScreenSchemeEntity issueTypeScreenSchemeEntity = new IssueTypeScreenSchemeEntityImpl(issueTypeScreenSchemeManager, (GenericValue) null, fieldScreenSchemeManager, constantsManager);
        issueTypeScreenSchemeEntity.setIssueTypeId(null);
        issueTypeScreenSchemeEntity.setFieldScreenScheme(fieldScreenSchemeManager.getFieldScreenScheme(screenSchemeId));
        issueTypeScreenScheme.addEntity(issueTypeScreenSchemeEntity);

        return issueTypeScreenScheme;
    }

}
