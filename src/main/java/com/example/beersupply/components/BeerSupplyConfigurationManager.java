package com.example.beersupply.components;

import java.util.List;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import com.example.beersupply.ao.BeerStyleService;
import com.example.beersupply.brewerydb.BreweryDBClient;
import com.example.beersupply.brewerydb.representations.Style;
import com.example.beersupply.exception.BeerSupplySetupException;

import org.apache.commons.lang.StringUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since version
 */
public final class BeerSupplyConfigurationManager
{
    private static final String SETTINGS_PROJECT_KEY = "com.example.beersupply:projectKey";
    private static final String SETTINGS_BREWERYDB_KEY = "com.example.beersupply:breweryDBKey";
    
    /**
     * The ActiveObjects component will be injected
     */
    private final PluginSettingsFactory pluginSettingsFactory;
    private final BeerSupplyProjectCreator projectCreator;
    private final BeerSupplyWorkflowCreator workflowCreator;
    private final BeerSupplyScreenCreator screenCreator;
    private final BeerStyleService beerStyleService;
    private final BreweryDBClient breweryDBClient;

    public BeerSupplyConfigurationManager(PluginSettingsFactory pluginSettingsFactory, BeerSupplyProjectCreator projectCreator, BeerSupplyWorkflowCreator workflowCreator, BeerSupplyScreenCreator screenCreator,BeerStyleService beerStyleService, BreweryDBClient breweryDBClient)
    {
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory);
        this.projectCreator = projectCreator;
        this.workflowCreator = workflowCreator;
        this.screenCreator = screenCreator;
        this.beerStyleService = beerStyleService;
        this.breweryDBClient = breweryDBClient;
    }

    public boolean isProjectConfigured()
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        return StringUtils.isNotBlank((String) settings.get(SETTINGS_PROJECT_KEY));
    }

    public boolean hasApiKey()
    {
        return StringUtils.isNotBlank(getBreweryDBApiKey());
    }

    public void runSetup(User user) throws BeerSupplySetupException
    {
        Project project = projectCreator.createProject(user);

        workflowCreator.createWorkflow(project,user);

        screenCreator.createScreenAndSchemes(project);

        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        settings.put(SETTINGS_PROJECT_KEY,project.getKey());
    }

    public String getBreweryDBApiKey()
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        return (String)settings.get(SETTINGS_BREWERYDB_KEY);
    }

    public void setBreweryDBApiKey(String key)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        settings.put(SETTINGS_BREWERYDB_KEY,key);

        if(beerStyleService.getCount() < 10)
        {
            List<Style> styles = breweryDBClient.getAllBeerStyles(key);
            for(Style style : styles)
            {
                beerStyleService.add(style.getName());
            }
        }
    }

}
