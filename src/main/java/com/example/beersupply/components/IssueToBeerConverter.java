package com.example.beersupply.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.message.I18nResolver;

import com.example.beersupply.jira.customfields.BeerAverageRatingCFType;
import com.example.beersupply.jira.customfields.BeerLabelCFType;
import com.example.beersupply.jira.customfields.BeerStyleCFType;
import com.example.beersupply.model.Beer;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;


/**
 * @since version
 */
public class IssueToBeerConverter
{
    private final ProjectManager projectManager;
    private final IssueManager issueManager;
    private final CustomFieldManager customFieldManager;
    private final I18nResolver i18n;
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final BeerRatingManager beerRatingManager;

    public IssueToBeerConverter(ProjectManager projectManager, IssueManager issueManager, CustomFieldManager customFieldManager, I18nResolver i18n, WebResourceUrlProvider webResourceUrlProvider, BeerRatingManager beerRatingManager)
    {
        this.projectManager = projectManager;
        this.issueManager = issueManager;
        this.customFieldManager = customFieldManager;
        this.i18n = i18n;
        this.webResourceUrlProvider = webResourceUrlProvider;
        this.beerRatingManager = beerRatingManager;
    }

    public List<Beer> getBeersFromIssues(User user) throws GenericEntityException
    {
        String projectKey = i18n.getText("beer-supply.project.key");
        Project project = projectManager.getProjectObjByKey(projectKey);

        Collection<Long> issueIds = issueManager.getIssueIdsForProject(project.getId());

        if(null == issueIds || issueIds.isEmpty())
        {
            return Collections.<Beer> emptyList();
        }

        List<Issue> issues = issueManager.getIssueObjects(issueIds);

        List<Beer> beers = new ArrayList<Beer>(issues.size());

        for(Issue issue : issues)
        {
            String key = issue.getKey();
            String name = issue.getSummary();
            String desc = issue.getDescription();

            Double average = getAverage(issue);
            String style = getStyle(issue);
            String labelUrl = getLabelUrl(issue);

            Double userRating = beerRatingManager.getUserRating(key,user);

            beers.add(new Beer(key,name,style,desc,labelUrl,average,userRating));
        }

        return beers;
    }

    private String getLabelUrl(Issue issue)
    {
        String label = "";

        final CustomField labelField = Iterables.find(customFieldManager.getCustomFieldObjects(issue), new Predicate<CustomField>()
        {
            @Override
            public boolean apply(@Nullable CustomField input)
            {
                if (null != input && input.getCustomFieldType() instanceof BeerLabelCFType)
                {
                    return true;
                }
                return false;
            }
        });

        String issueLabel = (String) labelField.getValue(issue);

        if(null != issueLabel)
        {
            label = issueLabel;
        }

        if (StringUtils.isBlank(label))
        {
            label = webResourceUrlProvider.getStaticPluginResourceUrl("com.example.beersupply.atlassian-beer-supply:beer-supply-resources", "default-beer-label.png", UrlMode.ABSOLUTE);
        }

        return label;
    }

    private String getStyle(Issue issue)
    {
        String style = "";

        final CustomField styleField = Iterables.find(customFieldManager.getCustomFieldObjects(issue), new Predicate<CustomField>()
        {
            @Override
            public boolean apply(@Nullable CustomField input)
            {
                if (null != input && input.getCustomFieldType() instanceof BeerStyleCFType)
                {
                    return true;
                }
                return false;
            }
        });

        String issueStyle = (String) styleField.getValue(issue);
        if(null != issueStyle)
        {
            style = issueStyle;
        }

        return style;
    }

    private Double getAverage(Issue issue)
    {
        Double average = 0.0;
        final CustomField averageRatingField = Iterables.find(customFieldManager.getCustomFieldObjects(issue), new Predicate<CustomField>()
        {
            @Override
            public boolean apply(@Nullable CustomField input)
            {
                if (null != input && input.getCustomFieldType() instanceof BeerAverageRatingCFType)
                {
                    return true;
                }
                return false;
            }
        });

        Double issueAverage = (Double) averageRatingField.getValue(issue);
        if(null != issueAverage)
        {
            average = issueAverage;
        }

        return average;
    }

}
