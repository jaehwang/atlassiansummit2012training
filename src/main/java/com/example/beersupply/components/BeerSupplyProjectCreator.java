package com.example.beersupply.components;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarImpl;
import com.atlassian.jira.avatar.AvatarManager;
import com.atlassian.jira.avatar.TemporaryAvatar;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.context.manager.JiraContextTreeManager;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.fields.ConfigurableField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.config.manager.IssueTypeSchemeManager;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.permission.PermissionSchemeManager;
import com.atlassian.jira.project.AssigneeTypes;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.sal.api.message.I18nResolver;

import com.example.beersupply.exception.BeerSupplySetupException;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.ofbiz.core.entity.GenericEntityException;

/**
 * @since version
 */
public final class BeerSupplyProjectCreator
{
    private final I18nResolver i18n;
    private final ProjectService projectService;
    private final IssueTypeManager issueTypeManager;
    private final IssueTypeSchemeManager issueTypeSchemeManager;
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final JiraContextTreeManager treeManager;
    private final ProjectManager projectManager;
    private final ConstantsManager constantsManager;
    private final FieldConfigSchemeManager configSchemeManager;
    private final FieldManager fieldManager;
    private final PermissionSchemeManager permissionSchemeManager;
    private final AvatarManager avatarManager;

    public BeerSupplyProjectCreator(I18nResolver i18n, ProjectService projectService, IssueTypeManager issueTypeManager, IssueTypeSchemeManager issueTypeSchemeManager, WebResourceUrlProvider webResourceUrlProvider, ProjectManager projectManager, ConstantsManager constantsManager, FieldConfigSchemeManager configSchemeManager, FieldManager fieldManager, PermissionSchemeManager permissionSchemeManager, AvatarManager avatarManager)
    {
        this.i18n = i18n;
        this.projectService = projectService;
        this.issueTypeManager = issueTypeManager;
        this.issueTypeSchemeManager = issueTypeSchemeManager;
        this.webResourceUrlProvider = webResourceUrlProvider;

        this.projectManager = projectManager;
        this.constantsManager = constantsManager;
        this.permissionSchemeManager = permissionSchemeManager;
        this.avatarManager = avatarManager;

        this.treeManager = new JiraContextTreeManager(projectManager, constantsManager);
        this.configSchemeManager = configSchemeManager;
        this.fieldManager = fieldManager;
    }

    public Project createProject(User user) throws BeerSupplySetupException
    {
        //First, create the project
        String projectName = i18n.getText("beer-supply.project.name");
        String projectKey = i18n.getText("beer-supply.project.key");
        String projectDesc = i18n.getText("beer-supply.project.desc");

        Project project = createProject(user, projectName, projectKey, projectDesc);
        //Now add our custom issue type
        String typeName = i18n.getText("beer-supply.issuetype.name");
        String typeDesc = i18n.getText("beer-supply.issuetype.desc");
        String typeIcon = webResourceUrlProvider.getStaticPluginResourceUrl("com.example.beersupply.atlassian-beer-supply:beer-supply-resources", "issuetype-icon.png", UrlMode.ABSOLUTE);

        IssueType issueType = createIssueType(typeName, typeDesc, typeIcon);

        //now create the IssueTypeScheme with our one issue type
        String schemeName = i18n.getText("beer-supply.issuetype.scheme.name");
        String schemeDesc = i18n.getText("beer-supply.issuetype.scheme.desc");

        FieldConfigScheme configScheme = createIssueTypeScheme(schemeName,schemeDesc,issueType.getId());

        //associate the scheme with our project
        associateConfigScheme(project, configScheme);

        return project;
    }

    private FieldConfigScheme createIssueTypeScheme(final String schemeName,final String schemeDesc,final String issueTypeId)
    {
        FieldConfigScheme configScheme = Iterables.find(issueTypeSchemeManager.getAllSchemes(),new Predicate<FieldConfigScheme>() {
            @Override
            public boolean apply(@Nullable FieldConfigScheme input)
            {
                return input.getName().equals(schemeName);
            }
        },null);

        if(null == configScheme)
        {
            configScheme = issueTypeSchemeManager.create(schemeName, schemeDesc, new ArrayList<String>(Arrays.asList(issueTypeId)));
            issueTypeSchemeManager.setDefaultValue(configScheme.getOneAndOnlyConfig(), issueTypeId);
        }

        return configScheme;
    }

    private FieldConfigScheme associateConfigScheme(Project project, FieldConfigScheme configScheme) throws BeerSupplySetupException
    {
        try
        {
            List contexts = CustomFieldUtils.buildJiraIssueContexts(false, null, new Long[]{project.getId()}, treeManager);
            ConfigurableField configurableField = fieldManager.getConfigurableField(IssueFieldConstants.ISSUE_TYPE);
            configScheme = configSchemeManager.updateFieldConfigScheme(configScheme, contexts, configurableField);

            fieldManager.refresh();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            SimpleErrorCollection errorCollection = new SimpleErrorCollection();
            errorCollection.addErrorMessage(e.getMessage());
            throw new BeerSupplySetupException(i18n.getText("beer-supply.project.creation.error"), errorCollection);
        }

        return configScheme;
    }

    private IssueType createIssueType(final String name, final String desc, final String url) throws BeerSupplySetupException
    {
        IssueType issueType = Iterables.find(issueTypeManager.getIssueTypes(),new Predicate<IssueType>() {
            @Override
            public boolean apply(@Nullable IssueType input)
            {
                return input.getName().equals(name);
            }
        },null);

        if(null == issueType)
        {
            try
            {
                issueType = issueTypeManager.createIssueType(name, desc, url);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SimpleErrorCollection errorCollection = new SimpleErrorCollection();
                errorCollection.addErrorMessage(e.getMessage());
                throw new BeerSupplySetupException(i18n.getText("beer-supply.project.creation.error"), errorCollection);
            }
        }

        return issueType;
    }

    private Project createProject(User user, String name, String key, String desc) throws BeerSupplySetupException
    {
        Project project = projectService.getProjectByKey(user, key).getProject();

        if (null == project)
        {

            ProjectService.CreateProjectValidationResult validation = projectService.validateCreateProject(user, name, key, desc, user.getName(), null, AssigneeTypes.PROJECT_LEAD);

            //need to add the default permission scheme
            ProjectService.UpdateProjectSchemesValidationResult schemesResult = projectService.validateUpdateProjectSchemes(user, getDefaultPermissionSchemeId(), null, null);

            ErrorCollection errorCollection = new SimpleErrorCollection();
            errorCollection.addErrorCollection(validation.getErrorCollection());
            errorCollection.addErrorCollection(schemesResult.getErrorCollection());

            if (!errorCollection.hasAnyErrors())
            {
                project = projectService.createProject(validation);
                projectService.updateProjectSchemes(schemesResult, project);

                Avatar finalAvatar = null;
                Long avatarId;
                try
                {
                    //create our avatar
                    InputStream is = getClass().getResourceAsStream("/images/beer-logo.png");
                    Avatar newAvatar = AvatarImpl.createCustomAvatar("beer-logo.png", "image/png", Avatar.Type.PROJECT, project.getId().toString());
                    finalAvatar = avatarManager.create(newAvatar,is,null);
                    avatarId = finalAvatar.getId();

                    projectManager.updateProject(project,project.getName(),project.getDescription(),project.getLead().getName(),project.getUrl(),project.getAssigneeType(),avatarId);
                }
                catch (IOException e)
                {
                    //ignore it
                }
            }
            else
            {
                throw new BeerSupplySetupException(i18n.getText("beer-supply.project.creation.error"), errorCollection);
            }
        }

        return project;
    }

    private Long getDefaultPermissionSchemeId()
    {
        try
        {
            return permissionSchemeManager.getDefaultScheme().getLong("id");
        }
        catch (GenericEntityException e)
        {
            return 0L;
        }
    }
}
