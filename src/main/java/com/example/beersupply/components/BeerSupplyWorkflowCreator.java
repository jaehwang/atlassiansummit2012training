package com.example.beersupply.components;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.Scheme;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.jira.workflow.WorkflowSchemeManager;
import com.atlassian.sal.api.message.I18nResolver;

import com.example.beersupply.exception.BeerSupplySetupException;

import org.ofbiz.core.entity.GenericValue;

/**
 * @since version
 */
public final class BeerSupplyWorkflowCreator
{
    private final I18nResolver i18n;
    private final WorkflowManager workflowManager;
    private final WorkflowSchemeManager workflowSchemeManager;

    public BeerSupplyWorkflowCreator(I18nResolver i18n, WorkflowManager workflowManager, WorkflowSchemeManager workflowSchemeManager)
    {
        this.i18n = i18n;
        this.workflowManager = workflowManager;
        this.workflowSchemeManager = workflowSchemeManager;
    }

    public JiraWorkflow createWorkflow(Project project, User user) throws BeerSupplySetupException
    {
        String workflowName = i18n.getText("beer-supply.workflow.name");
        String workflowDesc = i18n.getText("beer-supply.workflow.desc");

        JiraWorkflow workflow = createWorkflow(workflowName,workflowDesc,user);

        String workflowSchemeName = i18n.getText("beer-supply.workflow.scheme.name");
        String workflowSchemeDesc = i18n.getText("beer-supply.workflow.scheme.desc");

        createAndAssociateScheme(workflowSchemeName, workflowSchemeDesc, workflow, project);

        return workflow;
    }

    private void createAndAssociateScheme(String workflowSchemeName, String workflowSchemeDesc, JiraWorkflow workflow, Project project) throws BeerSupplySetupException
    {
        Scheme workflowScheme = null;
        try
        {
            workflowScheme = workflowSchemeManager.getSchemeObject(workflowSchemeName);
        }
        catch (Exception e)
        {
            workflowScheme = null;
        }

        if(null == workflowScheme)
        {
            try
            {
                workflowScheme = workflowSchemeManager.createSchemeObject(workflowSchemeName,workflowSchemeDesc);
                GenericValue schemeGV = workflowSchemeManager.getScheme(workflowScheme.getId());

                IssueType issueType = project.getIssueTypes().iterator().next();

                workflowSchemeManager.addWorkflowToScheme(schemeGV,workflow.getName(),issueType.getId());
                workflowSchemeManager.addSchemeToProject(project,workflowScheme);
                workflowSchemeManager.clearWorkflowCache();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SimpleErrorCollection errorCollection = new SimpleErrorCollection();
                errorCollection.addErrorMessage(e.getMessage());
                throw new BeerSupplySetupException(i18n.getText("beer-supply.project.creation.error"),errorCollection);
            }
        }
    }

    private JiraWorkflow createWorkflow(String workflowName, String workflowDesc, User user)
    {
       
        JiraWorkflow workflow;

        if(!workflowManager.workflowExists(workflowName))
        {
            workflow = new ConfigurableJiraWorkflow(workflowName,workflowManager);
            ((ConfigurableJiraWorkflow)workflow).setDescription(workflowDesc);

            workflowManager.createWorkflow(user,workflow);
        }
        else
        {
            workflow = workflowManager.getWorkflow(workflowName);
        }

        return workflow;
    }
}
