package com.example.beersupply.rest;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import com.example.beersupply.brewerydb.BreweryDBClient;
import com.example.beersupply.brewerydb.representations.BeerDescriptor;
import com.example.beersupply.components.BeerRatingManager;
import com.example.beersupply.components.BeerSupplyConfigurationManager;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
@Path("/beer")
public class BeerSupplyResource
{
    private final BeerRatingManager ratingManager;
    private final JiraAuthenticationContext authContext;
    private final BreweryDBClient breweryDBClient;
    private final BeerSupplyConfigurationManager configManager;
    private final WebResourceUrlProvider webResourceUrlProvider;
    
    public BeerSupplyResource(BeerRatingManager ratingManager, JiraAuthenticationContext authContext, BreweryDBClient breweryDBClient, BeerSupplyConfigurationManager configManager, WebResourceUrlProvider webResourceUrlProvider)
    {
        this.ratingManager = ratingManager;
        this.authContext = authContext;
        this.breweryDBClient = breweryDBClient;
        this.configManager = configManager;
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("/{issueKey}/rate")
    public Response setRating(@PathParam("issueKey") String issueKey, Double rating)
    {
        return null;
        // Summit 2012 training: Implement me!
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("/{issueKey}/rating/user")
    public Response getUserRating(@PathParam("issueKey") String issueKey)
    {
        return null;
        // Summit 2012 training: Implement me!
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("/{issueKey}/rating/average")
    public Response getAverageRating(@PathParam("issueKey") String issueKey)
    {
        return null;
        // Summit 2012 rating: Implement me!
    }
    
    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    @Path("/search/{searchTerm}")
    public Response searchForBeers(@PathParam("searchTerm") String searchTerm)
    {
        List<BeerDescriptor> beers = breweryDBClient.search(searchTerm, configManager.getBreweryDBApiKey());
        
        //make sure we have images for labels
        for(BeerDescriptor beer : beers)
        {
            if(StringUtils.isBlank(beer.getIcon()))
            {
                String beerIcon = webResourceUrlProvider.getStaticPluginResourceUrl("com.example.beersupply.atlassian-beer-supply:beer-supply-resources", "default-beer-icon.png", UrlMode.ABSOLUTE);
                beer.setIcon(beerIcon);
            }

            if(StringUtils.isBlank(beer.getLabel()))
            {
                String beerLabel = webResourceUrlProvider.getStaticPluginResourceUrl("com.example.beersupply.atlassian-beer-supply:beer-supply-resources", "default-beer-label.png", UrlMode.ABSOLUTE);
                beer.setLabel(beerLabel);
            }
        }

        return Response.ok(beers).build();
    }
    
}
