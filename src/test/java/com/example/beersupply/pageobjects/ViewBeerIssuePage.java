package com.example.beersupply.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

/**
 * @since version
 */
public class ViewBeerIssuePage implements Page
{
    private final String issueKey;

    @Inject
    private AtlassianWebDriver webDriver;

    public ViewBeerIssuePage(String issueKey)
    {
        this.issueKey = issueKey;
    }

    @Override
    public String getUrl()
    {
        return "/browse/" + issueKey;
    }
    
    public boolean hasHelloMessage()
    {
        boolean success = true;
        try
        {
            WebElement helloDiv = webDriver.findElement(By.id("hello"));
            success = helloDiv.getText().equals("Hello Summit!");
        }
        catch (NoSuchElementException e)
        {
            success = false;
        }

        return success;
    }

    
    @WaitUntil
    public void waitUntilBody()
    {
        webDriver.waitUntilElementIsLocated(By.id("beer-supply-rating-web-panel"));

    }
    
    public void clickRadio1()
    {
        webDriver.findElement(By.id("my1")).click();
    }

    public void clickRadio2()
    {
        webDriver.findElement(By.id("my2")).click();
    }

    public void clickRadio3()
    {
        webDriver.findElement(By.id("my3")).click();
    }

    public void clickRadio4()
    {
        webDriver.findElement(By.id("my4")).click();
    }

    public void clickRadio5()
    {
        webDriver.findElement(By.id("my5")).click();
    }

    public void clickRaty1()
    {
        webDriver.findElement(By.id("myRating")).findElement(By.xpath("img[1]")).click();
    }

    public void clickRaty2()
    {
        webDriver.findElement(By.id("myRating")).findElement(By.xpath("img[2]")).click();
    }

    public void clickRaty3()
    {
        webDriver.findElement(By.id("myRating")).findElement(By.xpath("img[3]")).click();
    }

    public void clickRaty4()
    {
        webDriver.findElement(By.id("myRating")).findElement(By.xpath("img[4]")).click();
    }

    public void clickRaty5()
    {
        webDriver.findElement(By.id("myRating")).findElement(By.xpath("img[5]")).click();
    }
}
