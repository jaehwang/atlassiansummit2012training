package com.example.beersupply.pageobjects;

import javax.inject.Inject;

import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @since version
 */
public class BeerSupplyAdminSetup
{
    @Inject
    private AtlassianWebDriver webDriver;

    public boolean getSuccess()
    {
        boolean success = true;
        try
        {
            webDriver.findElement(By.className("success"));
        }
        catch (NoSuchElementException e)
        {
            success = false;
        }
        
        return success;
    }
}
