package com.example.beersupply.util;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.apache.wink.client.handlers.BasicAuthSecurityHandler;

/**
 * @since version
 */
public class IssueUtil
{
    private static String baseUrl = System.getProperty("baseurl") + "/rest/api/2/issue";
    
    public static String createBeerIssue(String summary) throws Exception
    {
        String resourceUrl = baseUrl;
        BasicAuthHandler authHandler = new BasicAuthHandler("admin","admin");
        
        ClientConfig config = new ClientConfig();
        config.handlers(authHandler);
        
        RestClient restClient = new RestClient(config);
        
        Resource resource = restClient.resource(resourceUrl);

        resource.contentType(MediaType.APPLICATION_JSON_TYPE);
        resource.accept(MediaType.APPLICATION_JSON_TYPE);

        String json = "{\"fields\":{\"project\":{\"key\":\"BEERSUPPLY\"},\"summary\":\"" + summary + "\",\"issuetype\":{\"name\": \"Beer\"}}}";

        String issueJson = resource.post(String.class,json);
        JsonObject issue = new JsonParser().parse(issueJson).getAsJsonObject();
        
        return issue.get("key").getAsString();

    }
    
    
}
